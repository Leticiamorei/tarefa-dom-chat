let notas = document.querySelector(".entradaDeNotas");
let valores = document.querySelector(".listaNum");
let add = document.querySelector("#btnAdicionar");
let cont = 0;
let soma = 0;
let botaoCalculaMedia = document.querySelector("#btnMedia")
let textoResultado = document.querySelector(".result")

function adicionar_notas(){
    if (notas.value == ""){
        alert("Por favor, insira uma nota")
    }
    else if(notas.value < 0 || notas.value > 10){
        alert("A nota digitada é inválida, por favor, insira uma nota válida")
    }
    else{
        
        cont += 1
        valores.innerText = valores.innerText + "A nota " + cont + " foi " + notas.value + " \n" 

        soma = soma + parseFloat(notas.value)
        console.log(parseFloat(notas.value))
    }

}

function calcula_media(){
    textoResultado.innerText = (soma / cont).toFixed(2)

}


add.addEventListener('click',() => {adicionar_notas()})
botaoCalculaMedia.addEventListener('click', () => {calcula_media()} )
